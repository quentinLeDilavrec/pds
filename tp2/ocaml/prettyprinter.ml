open ASD

let rec typ_to_string = function
  | Type_Int -> "INT"
  | Type_Void -> "VOID"
  | Type_Pointer p -> "*"^(typ_to_string p)
  | Type_Array (i,t) -> "["^(string_of_int i)^" x "^(typ_to_string t)

let rec expr_to_string tab : expression -> string =
  let f = function | e -> expr_to_string (tab^"  ") e
  in (function
  | AddExpression (e1, e2)  -> tab^"+\n"^(f e1)^(f e2)
  | SubExpression (e1, e2) -> tab^"-\n"^(f e1)^(f e2)
  | MulExpression (e1, e2)  -> tab^"*\n"^(f e1)^(f e2)
  | DivExpression (e1, e2)  -> tab^"/\n"^(f e1)^(f e2)
  | IntegerExpression (i)   -> tab^(string_of_int i)^"\n"
  | IdentExpression(s,None)   -> tab^s^"\n"
  | IdentExpression(s,Some e)   -> tab^s^"\n"^(f e)
  | QuoteExpression s   -> tab^"\""^s^"\""^"\n"
  | CallExpression (i, el)  -> tab^i^"\n"^(String.concat "" (List.map f el)))


(* let rec instr_to_string tab : block -> string =
 *   let f = function | e -> expr_to_string (tab^"  ") e
 *   and g = function | e -> instr_to_string (tab^"  ") e
 *   in (function
 *   | Assign (i, e) -> tab^":=\n"^(f i)^(f e)
 *   | Return (e)    -> tab^"ret\n"^(f e)
 *   | Declaration(t,l)    -> tab^"decl "^(typ_to_string t)^"\n"^(String.concat "" (List.map f l))
 *   | Print(l)    -> tab^"print\n"^(String.concat "" (List.map f l))
 *   | Read(l)    -> tab^"print\n"^(String.concat "" (List.map f l))
 *   | b -> raise (block_to_string b)^" not an instruction" *)
  (* | ReturnExpression (e)    -> tab^"ret\n"^(f e)
   * | Proto (t, i , l)        -> tab^(String.concat " " ("proto"::(typ_to_string t)::i::l))^"\n"
   * | Func (t, i , l , e)     -> tab^(String.concat " " ("proto"::(typ_to_string t)::i::l))^"\n"^(f e)
   * | Declaration (t, i , Some _nat)  -> tab^(typ_to_string t)^" "^i^"["^(string_of_int _nat)^"]\n"
   * | Print (l) -> "print"
   * | Read (i) -> "read"
   * | Block (l) -> String.concat "" (List.map f l)
   * | While (c, e) -> "while\n"^(f c)^(f e)
   * | Cond(c,t,e) -> "if\n"^(f c)^(f t)^(f e))*)
(* ) *)

let rec block_to_string tab : block -> string =
  let f = function | e -> expr_to_string (tab^"  ") e
  and g = function | e -> block_to_string (tab^"  ") e
  in (function
  | Assign (i, e) -> tab^":=\n"^(f i)^(f e)
  | Return (e)    -> tab^"ret\n"^(f e)
  | Declaration(t,l)    -> tab^"decl "^(typ_to_string t)^"\n"^(String.concat "" (List.map f l))
  | Print(l)    -> tab^"print\n"^(String.concat "" (List.map f l))
  | Read(l)    -> tab^"print\n"^(String.concat "" (List.map f l))
  | Call (i, el)  -> tab^i^"\n"^(String.concat "" (List.map f el))
  | Func(t,i,el,Some il) -> tab^"func\n"^(String.concat "" (List.map f el))^(String.concat "" (List.map g il))
  | Func(t,i,el,None) -> tab^"func\n"^(String.concat "" (List.map f el))
  | While (c, e) -> "while\n"^(f c)^(String.concat "" (List.map g e))
  | Cond(c,t,e) -> "if\n"^(f c)^
            (String.concat "" (List.map g t))^
            (match e with
                | Some e -> (String.concat "" (List.map g e))
                | None -> "")
  | _ -> failwith "not a block nor instr"
  (* | Block il -> tab^"block\n"^(String.concat "" (List.map g il)) *)
)

let to_string ast = (String.concat "" (List.map (block_to_string "") ast))

let prettyprint = to_string
