(* TODO : extend when you extend the language *)

type ident = string

type typ =
  | Type_Int
  | Type_Void
  | Type_Pointer of typ
  | Type_Array of int * typ

type expression =
  | AddExpression of expression * expression
  | SubExpression of expression * expression
  | MulExpression of expression * expression
  | DivExpression of expression * expression
  | IntegerExpression of int
  | IdentExpression of string * (expression option)
  | QuoteExpression of string
  | CallExpression of ident * expression list

(* The first 6 are leafs thus instructions *)
type block =

(* instr *)
  | Assign of expression * expression
  | Return of expression
  | Declaration of typ * (expression list)
  | Print of expression list
  | Read of expression list
  | Call of ident * expression list

(* block *)
  | While of expression * (block list)
  | Cond of expression * (block list) * (block list option)
  | Func of typ * ident * (expression list) * (block list option)


type program = block list

