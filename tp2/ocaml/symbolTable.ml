open List
open ASD

(* This file contains the symbol table definition. *)
(* A symbol table contains a set of ident and the  *)
(* corresponding symbols.                          *)
(* The order is important: this first match count  *)

type function_symbol_state = Defined | Declared

type function_symbol = {
  return_type: typ;
  identifier: ident;
  arguments: symbol_table;
  state: function_symbol_state;
}

and symbol =
  | VariableSymbol of typ * ident
  | VariableRedirection of ident * symbol
  | FunctionSymbol of function_symbol

and symbol_table = symbol list


(* public interface *)
let rec lookup (id:string) : symbol_table -> symbol option =
   (* let _ = List.find (fun x -> true) [1] in *)
   let rec assoc = function
    | ((VariableRedirection (key, r))) :: q
    | ((VariableSymbol (_, key)) as r) :: q
    | (FunctionSymbol {identifier = key; _} as r) :: q ->
        if key = id then
          Some r
        else
          assoc q
    | [] -> None
  in assoc

let add tab sym = sym :: tab

(* Note : obviously not symmetric *)
let merge = (@)

let rec prerr = function
  | [] -> prerr_endline "nothing in table"
  | (VariableSymbol (t, s))::l -> prerr_endline ((Prettyprinter.typ_to_string t)^" "^s) ; prerr l
  | (VariableRedirection (t, s))::l -> prerr_endline (t^"->")
  | (FunctionSymbol {
      return_type= typ;
      identifier= ident;
      arguments= symbol_table;
      state= function_symbol_state;
                        })::l -> prerr_endline "impl print symb table" ; prerr l
