open ASD
open Llvm
open Utils
open List
open SymbolTable


(* main function. return only a string: the generated code *)
let rec ir_of_ast (p:ASD.program) : string =
  (* this header describe to LLVM the target
   * and declare the external function printf
  *)
  let head = "; Target\n" ^
             "target triple = \"x86_64-unknown-linux-gnu\"\n" ^
             "; External declaration of the printf function\n" ^
             "declare i32 @printf(i8* noalias nocapture, ...)\n" ^
             "declare i32 @scanf(i8* noalias nocapture, ...)\n" ^
             "\n; Actual code begins\n\n"

  in let (hd,fl,_) = List.fold_left scope0_of_func ([], [], []) p
  in head^
     ( String.concat "\n\n"
         (List.rev_map (fun a -> Llvm.string_of_instr a) hd)
     )^"\n\n"^
     ( String.concat "\n\n"
         (List.rev_map (fun a -> Llvm.string_of_func a) fl)
     )^"\\n"
(* TODO : change when you extend the language *)
(* in let ((ir, _), (ret, _)) = ir_of_expression (empty_ir, []) p
 * (\* adds the return instruction *\)
 * in let new_ir = {
 *   header = ir.header;
 *   code = Return {
 *     ret_type = (LLVM_Type_Int 32);
 *     ret_value = ret;
 *   } :: ir.code
 * }
 * 
 * (\* generates the final string *\)
 * in head ^
 * 
 * (\* We create the function main *\)
 * (\* TODO : remove this when you extend the language *\)
 * "define i32 @main() {\n" ^
 * 
 * (string_of_ir new_ir) ^
 * 
 * (\* TODO : remove this when you extend the language *\)
 * (\* TODO : remove this when you extend the language *\)
 * "}\n" *)

and llvm_type_of_asd_typ = function
  | Type_Int -> LLVM_Type_Int 32
  | Type_Void -> LLVM_Type_Void
  | Type_Pointer p -> LLVM_Type_Pointer (llvm_type_of_asd_typ p)
  | Type_Array (i,t) -> LLVM_Type_Array (i,llvm_type_of_asd_typ t)

(* All main code generation functions take the current IR and a scope,
 * append header and/or code to the IR, and/or change the scope
 * They return the new pair (ir, scope)
 * This is convenient with List.fold_left
 *
 * They can return other stuff (synthesized attributes)
 * They can take extra arguments (inherited attributes)
*)


(* returns the regular pair, plus the pair of the name of the result and its type *)
and ir_of_expression (ir, scope) : ASD.expression -> ((llvm_ir * SymbolTable.symbol_table)*(string * llvm_type)) =
  (* function to generate all binop operations *)
  let aux op t (l, r) =
    (* generation of left operand computation. We give directly (ir, scope) *)
    let ll, (lresult_name, lresult_type) = ir_of_expression (ir, scope) l
    (* generation of right operand computation. We give directly (ir, scope) from the left computation *)
    (* it appends code to the previous code generated *)
    in let rr, (rresult_name, rresult_type) = ir_of_expression ll r

    (* allocate a new unique locale identifier *)
    and result = newtmp ()

    (* type checking *)
    in let _ = if lresult_type <> rresult_type || t <> rresult_type then failwith "Type error"

    (* new instruction *)
    in let code = Binop {
        lvalue_name = result;
        lvalue_type = t;
        op = op;
        left = lresult_name;
        right = rresult_name;
      }

    (* Returns : *)
    in (({
        header = (fst rr).header;
        code = code :: (fst rr).code;
      }, scope), (result, t))

  in function
    | AddExpression (l, r)  -> aux "add" (LLVM_Type_Int 32) (l, r)
    | SubExpression (l, r)  -> aux "sub" (LLVM_Type_Int 32) (l, r)
    | MulExpression (l, r)  -> aux "mul" (LLVM_Type_Int 32) (l, r)
    | DivExpression (l, r)  -> aux "div" (LLVM_Type_Int 32) (l, r)
    | IntegerExpression i    -> ((ir, scope), (string_of_int i, (LLVM_Type_Int 32)))
    | IdentExpression(s,None)    -> (  (*TODO get value of whole table*)
        match SymbolTable.lookup ("%"^s) scope with
        | Some (VariableSymbol(t,ident)) -> ((ir, scope), (ident, llvm_type_of_asd_typ t))
        | None -> failwith "not declared"; ((ir,scope), ("%"^s, LLVM_Type_Void))
        | Some (VariableRedirection (_,VariableSymbol (t,ident))) ->  ((ir, scope), (ident, llvm_type_of_asd_typ t))
        | _ -> failwith "not a valid type, need variable"
        (*TODO load*)
      )
    | IdentExpression(s,Some e)    ->
      (
        match SymbolTable.lookup ("%"^s) scope with
        | Some (VariableSymbol(t,s)) ->
          let (({code=code;header=header}, scope),
               (result, et)) = ir_of_expression (ir,scope) e
          in let addr = newtmp ()
          in let ty = llvm_type_of_asd_typ t
          in let code =
               (GetElementPtr {
                   result = addr;
                   ty = ty;
                   ptrval = s;
                   idx = [(et,result)];
                 })::code
          in let value = newtmp ()
          in let ty = ty;
               (* (match ty with LLVM_Type_Pointer p -> p); *)
          in let code =
               (Load {
                   result = value;
                   ty = ty;
                   pointer = addr;
                 })::code
          in (({code=code;header=header}, scope), (value, ty))
        | None -> failwith "not declared";((ir,scope), ("%"^s, LLVM_Type_Void))
        | _ -> failwith "not a valid type, need variable"
      )
    | CallExpression(s,el)    -> (
        match SymbolTable.lookup (s) scope with
        | Some (FunctionSymbol v) ->
          let f (acc1,acc2) x= (let (a,(i,t)) = ir_of_expression acc1 x in (a,(t,i)::acc2))
          in let (({code=code;header=header}, scope),
                  results) = List.fold_left f ((ir,scope),[]) el (*TODO check el=v.arguments*)
          in let addr = newtmp ()
          in let code = (Call {
              result=Some addr;
              fnptrval="@"^v.identifier;
              fnty=LLVM_Type_Func (llvm_type_of_asd_typ v.return_type,
                                   (List.map (function VariableSymbol(t,i) -> llvm_type_of_asd_typ t) v.arguments));
              args=results
            })::code
          in (({header=header;code=code},scope),(addr,llvm_type_of_asd_typ v.return_type))
        | None -> failwith (s^" was not declared");
        | _ -> failwith "not a valid type, need variable"
      )
    | _ -> failwith "expression not handled"
and ir_of_instr (ir, scope) : ASD.block -> (llvm_ir * SymbolTable.symbol_table) = function
  | Return e -> (let ((ir,scope),(result,t)) = ir_of_expression (ir, scope) e
                 in let ir ={ header = ir.header;
                              code = Return {
                                  ret_type = t;
                                  ret_value = result;
                                } :: ir.code
                            }
                 in (ir,scope))
  | Assign (e1,e2) -> ( (*TODO assign value to whole table*)
      match e1 with
      | IdentExpression(s,Some e) -> (
          let ((e2ir,e2scope),(e2result,e2t)) = ir_of_expression (ir, scope) e2
          in let ((e1ir,e1scope),(e1result,e1t)) = ir_of_expression (e2ir, e2scope) e
          in let (t,s) = (match SymbolTable.lookup ("%"^s) e2scope with
              | Some (VariableSymbol (t,s)) -> (t,s)
              | Some (FunctionSymbol {return_type=t; identifier=s}) -> (t,s)
              | Some (VariableRedirection (_,VariableSymbol (t,s))) -> (t,s)
              | Some (VariableRedirection (_,FunctionSymbol {return_type=t; identifier=s})) -> (t,s)
              | None -> failwith "trying to assign non declared value"
              | _ -> failwith "too deep")
          in let ptr_name = newtmp ()
          in let ir = { header = e1ir.header;
                        code =
                          (Store {
                              fty = e2t;
                              (* (match e2t with LLVM_Type_Pointer p -> p); *)
                              tty = e1t;
                              value = e2result;
                              pointer = ptr_name;
                            }) ::
                          (GetElementPtr {
                              result = ptr_name;
                              ty = llvm_type_of_asd_typ t;
                              ptrval = s;
                              idx = [(e1t,e1result)];
                            }) :: e1ir.code
                      }
          in (ir,e1scope))
      | IdentExpression(s,None) -> (
          let ((e2ir,e2scope),(e2result,e2t)) = ir_of_expression (ir, scope) e2
          in let _ = SymbolTable.prerr scope
          in let (t,s) = (match SymbolTable.lookup ("%"^s) e2scope with
              | Some (VariableSymbol (t,s)) -> (t,s)
              | Some (FunctionSymbol {return_type=t; identifier=s}) -> (t,s)
              | Some (VariableRedirection (_,VariableSymbol (t,s))) -> (t,s)
              | Some (VariableRedirection (_,FunctionSymbol {return_type=t; identifier=s})) -> (t,s)
              | None -> failwith "trying to assign non declared value")
          in let ir = { header = e2ir.header;
                        code = Store {
                            fty = e2t;
                            (* (match e2t with LLVM_Type_Pointer p -> p | a -> failwith (string_of_llvm_type a)); (\* TODO check fty = tty *\) *)
                            tty = llvm_type_of_asd_typ t;
                            value = e2result;
                            pointer = s;
                          } :: e2ir.code
                      }
          in (ir,e2scope))
      | _ -> failwith "bad lhs of assign")
  | Declaration(t, el) -> ( (* TODO multiple definitions*)
      let f (ir,scope) = (function
          | IdentExpression(s,None)    -> (
              let ir = { header = ir.header;
                         code = Alloca {
                             typ = llvm_type_of_asd_typ t;
                             pointer = "%"^s;
                           } :: ir.code
                       }
              in (ir,SymbolTable.add scope (VariableSymbol (t,"%"^s)))) (*TODO only ptr in table?*)
          | IdentExpression(s,Some e)    -> (
              let ((eir,escope),(eresult,et)) = ir_of_expression (ir, scope) e
              in let ir = { header = ir.header;
                            code = Alloca {
                                typ = LLVM_Type_Array(int_of_string eresult,
                                                      llvm_type_of_asd_typ t);
                                pointer = "%"^s;
                              } :: ir.code
                          }
              in (ir,SymbolTable.add scope (VariableSymbol (t,"%"^s))))
          | _ -> failwith "not a valid variable declaration")
      in List.fold_left f (ir,scope) el)
  | Print el ->
    (let f (sacc,vacc,(ir,scope)) = (function
         | QuoteExpression s -> (sacc^s,vacc,(ir,scope))
         | e -> let ((ir,scope),(res,t)) = ir_of_expression (ir,scope) e
           in (sacc^"%d",(t,res)::vacc,(ir,scope))) (* TODO other than %d *)
     in let (sacc,
             vacc,
             ({header=header;
               code=code},
              scope)) = List.fold_left f ("",[],(ir,scope)) el

     in let scope = SymbolTable.add scope (FunctionSymbol {(*TODO if not inside*)
         identifier="@printf";
         arguments=[];
         return_type=ASD.Type_Int;
         state=Declared;
       })
     in let global = newglob ".fmt"
     in let (sacc,len) = string_transform sacc
     in let sacc_t = LLVM_Type_Array (len, LLVM_Type_Int 8)
     in let header = (Global {
         globalVarName=global;
         _Type=sacc_t;
         initializerConstant="c\""^sacc^" \"";
       })::header
     in let call = (match SymbolTable.lookup "@printf" scope with | Some (FunctionSymbol v) -> v)
     in let code = (Call {
         result=None;
         fnptrval=call.identifier;
         fnty=LLVM_Type_Func (llvm_type_of_asd_typ call.return_type,
                              [LLVM_Type_Pointer (LLVM_Type_Int 8);
                               LLVM_Type_Ellipse]);
         args=(LLVM_Type_Pointer (LLVM_Type_Int 8),
               "getelementptr inbounds ("^
               (string_of_llvm_type sacc_t)^", "^
               (string_of_llvm_type (LLVM_Type_Pointer sacc_t))^" "^
               global^", i64 0, i64 0"^")")::(List.rev vacc);
       })::code
     in ({header=header;code=code},scope))
  | Read el ->
    (let f (sacc,vacc,(ir,scope)) = (function
         | e -> let ((ir,scope),(res,t)) = ir_of_expression (ir,scope) e
           in (sacc^"\n%d",(t,res)::vacc,(ir,scope))) (* TODO other than %d *)
     in let (sacc,
             vacc,
             ({header=header;
               code=code},
              scope)) = List.fold_left f ("",[],(ir,scope)) el

     in let scope = SymbolTable.add scope (FunctionSymbol {(*TODO if not inside*)
         identifier="@scanf";
         arguments=[];
         return_type=ASD.Type_Int;
         state=Declared;
       })
     in let global = newglob ".fmt"
     in let (sacc,len) = string_transform sacc
     in let sacc_t = LLVM_Type_Array (len, LLVM_Type_Int 8)
     in let header = (Global {
         globalVarName=global;
         _Type=sacc_t;
         initializerConstant="c\""^sacc^" \"";
       })::header
     in let call = (match SymbolTable.lookup "@scanf" scope with | Some (FunctionSymbol v) -> v)
     in let code = (Call {
         result=None;
         fnptrval=call.identifier;
         fnty=LLVM_Type_Func (llvm_type_of_asd_typ call.return_type,
                              [LLVM_Type_Pointer (LLVM_Type_Int 8);
                               LLVM_Type_Ellipse]);
         args=(LLVM_Type_Pointer (LLVM_Type_Int 8),
               "getelementptr inbounds ("^
               (string_of_llvm_type sacc_t)^", "^
               (string_of_llvm_type (LLVM_Type_Pointer sacc_t))^" "^
               global^", i64 0, i64 0"^")")::(List.rev vacc);
       })::code
     in ({header=header;code=code},scope))
  | Call(s,el)    -> (
      match SymbolTable.lookup (s) scope with
      | Some (FunctionSymbol v) ->
        let f (acc1,acc2) x= (let (a,(i,t)) = ir_of_expression acc1 x in (a,(t,i)::acc2))
        in let (({code=code;header=header}, scope),
                results) = List.fold_left f ((ir,scope),[]) el (*TODO check el=v.arguments*)
        in let code = (Call {
            result=None;
            fnptrval="@"^v.identifier;
            fnty=LLVM_Type_Func (llvm_type_of_asd_typ v.return_type,
                                 (List.map (function VariableSymbol(t,i) -> llvm_type_of_asd_typ t) v.arguments));
            args=results
          })::code
        in ({header=header;code=code},scope)
      | None -> failwith (s^" was not declared");
      | _ -> failwith "not a valid type, need variable"
    )

  (* | Call(s, el) -> let f acc x = acc
   *   in List.fold_left f (ir,scope) el *)
  | _ -> failwith "instr not matched"
(* | b -> raise (block_to_string b)^" not an instruction" *)

and function_symbol_of_asd t i el s : SymbolTable.function_symbol =
  {
    return_type = t;
    identifier = i;
    arguments = List.map (function
        | IdentExpression (i, Some _) -> VariableSymbol (Type_Array (0,Type_Int),i)
        | IdentExpression (i, None)   -> VariableSymbol (Type_Int,i)
        | _ -> failwith "fail in proto_of_asd, not an identifier"
      ) el;
    state = s
  }

and scope0_of_func (hd, fl, scope) : ASD.block -> (llvm_instr list * ((function_symbol * (llvm_ir option)) list) * SymbolTable.symbol_table) = function (*TODO test redeclaration*)
  | Func(t,i,el,Some ins) -> (*TODO test incompatible proto*)
    let fsymb = function_symbol_of_asd t (i) el SymbolTable.Defined
    in let (code,iscope) = List.fold_left (fun (acc1,acc2) -> function
        | IdentExpression (s, Some _) -> (
            (Load {
                result = "%"^s^"1";
                ty = LLVM_Type_Pointer (LLVM_Type_Int 32);
                pointer = "%"^s;
              })::
            (Alloca {
                typ = LLVM_Type_Pointer (LLVM_Type_Int 32);
                pointer = "%"^s^"1";
              })::acc1,
            (VariableRedirection(
                "%"^s,
                VariableSymbol (ASD.Type_Int,"%"^s^"1")))::acc2)
        | IdentExpression (s, None)   -> (
            (Load {
                result = "%"^s^"1";
                ty = LLVM_Type_Int 32;
                pointer = "%"^s;
              })::
            (Alloca {
                typ = LLVM_Type_Int 32;
                pointer = "%"^s^"1";
              })::acc1,
            (VariableRedirection(
                "%"^s,
                VariableSymbol (ASD.Type_Int,"%"^s^"1")))::acc2)
        | _ -> failwith "don't work for now"
      ) ([],scope) el
    in let ir={header=[];code=code}
    in let (ir, iscope) = List.fold_left ir_of_block2 (ir,iscope) ins
    in let ir = match t with
        | ASD.Type_Void -> {header=ir.header;
                            code=Llvm.Return {ret_type=llvm_type_of_asd_typ t;
                                              ret_value=""}::ir.code}
        | ASD.Type_Int -> {header=ir.header;
                           code=Llvm.Return {ret_type=llvm_type_of_asd_typ t;
                                             ret_value="0"}::ir.code}
        | _ -> ir
    in (ir.header@hd,(fsymb, Some ir)::fl, (SymbolTable.FunctionSymbol fsymb)::(scope))
  | Func(t,i,el,None)     ->
    let fsymb = function_symbol_of_asd t (i) el SymbolTable.Declared
    in (hd,(fsymb,None)::fl, (SymbolTable.FunctionSymbol fsymb)::scope)
  | _ -> failwith "not a top expression"

and ir_of_block2 (ir, scope) : ASD.block -> (llvm_ir * SymbolTable.symbol_table) = function
  | While(e,il) ->
    let whileL = newlab "while"
    in let code = (Br {
        dest=whileL;
      })::ir.code
    in let code = (Label whileL)::code
    in let ((ir,scope),(res,t)) = ir_of_expression ({code=code; header=ir.header}, scope) e
    in let cond_tmp = newtmp ()
    in let code = (Icmp {
        result=cond_tmp;
        cond="ne";
        ty=t;
        op1=res;
        op2="0";
      })::ir.code
    in let doL = newlab "do"
    in let doneL = newlab "done"
    in let code = (Br_cond {
        cond=cond_tmp;
        iftrue=doL;
        iffalse=doneL;
      })::code
    in let code = (Label doL)::code
    in let (ir,scope) =  List.fold_left ir_of_block2 ({code=code; header=ir.header}, scope) il
    in let code = (Br {
        dest=whileL;
      })::ir.code
    in let code = (Label doneL)::code
    in ({code=code; header=ir.header}, scope)
  | Cond(e,il,il2) ->
    let ((ir,scope),(res,t)) = ir_of_expression ({code=ir.code; header=ir.header}, scope) e
    in let cond_tmp = newtmp ()
    in let code = (Icmp {
        result=cond_tmp;
        cond="ne";
        ty=t;
        op1=res;
        op2="0";
      })::ir.code
    in let thenL = newlab "then"
    in let elseL = newlab "else"
    in let code = (Br_cond {
        cond=cond_tmp;
        iftrue=thenL;
        iffalse=elseL;
      })::code
    in let code = (Label thenL)::code
    in let (ir,scope) =  List.fold_left ir_of_block2 ({code=code; header=ir.header}, scope) il
    in let fiL = newlab "fi"
    in let code = (Br {
        dest=fiL;
      })::ir.code
    in let code = (Label elseL)::code
    in let (ir,scope) =  List.fold_left ir_of_block2 ({code=code; header=ir.header}, scope) il
    in let code = (Br {
        dest=fiL;
      })::ir.code
    in let code = (Label fiL)::code
    in ({code=code; header=ir.header}, scope)
  | _instr -> ir_of_instr (ir,scope) _instr
