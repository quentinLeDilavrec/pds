open Lexer
open Parser
open ASD
open Codegen
open Prettyprinter


let stream_tee stream =
    let next self other i =
      try
        if Queue.is_empty self
        then
          let value = Stream.next stream in
          Queue.add value other;
          Some value
        else
          Some (Queue.take self)
      with Stream.Failure -> None in
    let q1 = Queue.create () in
    let q2 = Queue.create () in
    (Stream.from (next q1 q2), Stream.from (next q2 q1))


let lexbuf = Lexing.from_channel stdin
in try
  let token_stream = (Stream.of_list (Lexer.tokenize lexbuf))
  in let token_stream, copy = stream_tee token_stream
  in prerr_endline ">\tcheck token stream:"; Stream.iter Token.prerr copy ; prerr_newline ();
     prerr_endline "starting parsing";
     Printexc.record_backtrace true;
     let ast = Parser.program token_stream

  (* Activate one of these output: pretty-print or LLVM IR *)

  (* Pretty-print input *)
  in prerr_endline (prettyprint ast)

  ;
  (* Print LLVM IR *)
  let document = ir_of_ast ast
  in print_endline document

with
  Lexer.Unexpected_character e ->
  begin
    Printf.printf "Unexpected character: `%c' at position '%d' on line '%d'\n"
      e.character e.pos e.line;
    exit 1
  end


