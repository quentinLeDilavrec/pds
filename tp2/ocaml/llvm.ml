(* TODO : extend when you extend the language *)

(* This file contains a simple LLVM IR representation *)
(* and methods to generate its string representation  *)

open List
open SymbolTable
open ASD

type llvm_type =
  | LLVM_Type_Int of int
  | LLVM_Type_Void
  | LLVM_Type_Pointer of llvm_type
  | LLVM_Type_Array of int * llvm_type
  | LLVM_Type_Func of llvm_type * (llvm_type list)
  | LLVM_Type_Ellipse

(* Warning: because of type inference, we can not
 * have the same field name in two record type
 * (actually this is possible but cumbersome)
*)

(* fields in each instruction *)
type llvm_binop = {
  lvalue_name: string;
  lvalue_type: llvm_type;
  op: string;
  left: string;
  right: string;
}

and llvm_return = {
  ret_type: llvm_type;
  ret_value: string; (* we only return identifier, or integers as int *)
}

and llvm_store = {
  fty: llvm_type;
  tty: llvm_type;
  value: string;
  pointer: string;
}

and llvm_indexed = {
  result: string;
  ty: llvm_type;
  ptrval: string;
  idx: (llvm_type * string) list;
}

and llvm_global = {
  globalVarName: string;
  _Type: llvm_type;
  initializerConstant: string;
}
and llvm_alloca = {
  typ: llvm_type;
  pointer: string;
  (* ty: llvm_type option;
   * numElements: int option; *)
}

and llvm_load = {
  result: string;
  ty: llvm_type;
  pointer: string;
}

and llvm_br = {
  dest: string;
}

and llvm_call = {
  (* result: string option; *)
  result: string option;
  fnptrval: string;
  fnty: llvm_type;
  args: (llvm_type*string) list;
}

and llvm_icmp = {
  result: string;
  cond: string;
  ty: llvm_type;
  op1: string;
  op2: string;
}

and llvm_br_cond = {
  cond: string;
  iftrue: string;
  iffalse: string;
}

and llvm_proto = {
  return_type: llvm_type;
  identifier: string;
  arguments: (llvm_type * string) list;
}
(* instructions sum type *)
and llvm_instr =
  | Binop of llvm_binop
  | Return of llvm_return
  | Store of llvm_store
  | Br of llvm_br
  | Br_cond of llvm_br_cond
  | Icmp of llvm_icmp
  | Alloca of llvm_alloca
  | Global of llvm_global
  | GetElementPtr of llvm_indexed
  | Load of llvm_load
  | Call of llvm_call
  | Label of string

(* Note: instructions in list are taken in reverse order in
 * string_of_ir in order to permit easy list construction !!
*)
and llvm_ir = {
  header: llvm_instr list; (* to be placed before all code (global definitions) *)
  code: llvm_instr list;
}

(* handy *)
let empty_ir = {
  header = [];
  code = [];
}


let rec llvm_type_of_ASD = function
  | Type_Int -> LLVM_Type_Int 32
  | Type_Void -> LLVM_Type_Void
  | Type_Pointer p -> LLVM_Type_Pointer (llvm_type_of_ASD p)
  | Type_Array (i,t) -> LLVM_Type_Array (i,llvm_type_of_ASD t)

(* actual IR generation *)
let rec string_of_llvm_type = function
  | LLVM_Type_Int v-> "i"^(string_of_int v)
  | LLVM_Type_Void -> "void"
  | LLVM_Type_Ellipse -> "..."
  | LLVM_Type_Pointer p -> (string_of_llvm_type p)^"*"
  | LLVM_Type_Func (r,p) -> (string_of_llvm_type r)^" ("^(String.concat ", "(List.map string_of_llvm_type p))^")"
  | LLVM_Type_Array(i,t) -> "["^(string_of_int i)^" x "^(string_of_llvm_type t)^"]"

and string_of_ir ir =
  (string_of_instr_list ir.header) ^ "\n\n" ^ (string_of_instr_list ir.code)

and string_of_instr_list l =
  String.concat "\n" (rev_map string_of_block l)

and string_of_block = function
  | Label v -> "\n"^v^":"
  | e -> "  "^(string_of_instr e)

and string_of_instr : llvm_instr -> string = function
  | Binop v -> v.lvalue_name ^ " = " ^ v.op ^ " " ^
               (string_of_llvm_type v.lvalue_type) ^ " " ^ v.left ^ ", " ^ v.right
  | Return v ->
    "ret " ^ (string_of_llvm_type v.ret_type) ^ " " ^ v.ret_value
  | Store v -> "store "^(string_of_llvm_type v.fty)^" "^v.value^", "^
               (string_of_llvm_type v.tty)^" "^v.pointer
  | Br v -> "br label %"^v.dest
  | Br_cond v -> "br i1 "^v.cond^", label %"^v.iftrue^", label %"^v.iffalse
  | Alloca v -> v.pointer^" = alloca "^(string_of_llvm_type v.typ)
  | Global v -> v.globalVarName^" = global "^(string_of_llvm_type v._Type)^" "^
                v.initializerConstant
  | Load v -> v.result^" = load "^(string_of_llvm_type v.ty)^", "^
              (string_of_llvm_type (LLVM_Type_Pointer v.ty))^" "^v.pointer
  | GetElementPtr v -> v.result^" = getelementptr "^
                       (string_of_llvm_type v.ty)^", "^
                       (string_of_llvm_type (LLVM_Type_Pointer v.ty))^" "^v.ptrval^", "^
                       (String.concat ", " (List.map (fun (t,s) ->
                            (string_of_llvm_type t)^" "^s) v.idx))
  | Call ({result=None} as v) ->  "call "^(string_of_llvm_type v.fnty)^" "^
                                         v.fnptrval^"("^(String.concat ", " (List.map (
      fun (t,s) ->(string_of_llvm_type t)^" "^s
    ) v.args))^")"
  | Call ({result=Some s} as v) ->  s^" = call "^(string_of_llvm_type v.fnty)^" "^
                                         v.fnptrval^"("^(String.concat ", " (List.map (
      fun (t,s) ->(string_of_llvm_type t)^" "^s
    ) v.args))^")"
  | Icmp v -> v.result^" = icmp "^v.cond^" "^(string_of_llvm_type v.ty)^" "^v.op1^", "^v.op2
  | _ -> failwith "instr not handled"
and string_of_proto def (p : function_symbol) : string =
  (if def then "define" else "declare")^" "^(string_of_llvm_type (llvm_type_of_ASD p.return_type))^" @"^p.identifier^
  "("^(String.concat ", "
         (List.map (function
              | (VariableSymbol(t,x)) ->
              let t = (match llvm_type_of_ASD t with
                  | LLVM_Type_Array (0,t) -> LLVM_Type_Pointer t
                  | a -> a)
              in (string_of_llvm_type t)^(if def then " %"^x else "")
              | _ -> failwith "only variable param for now") p.arguments))^")"
and string_of_func : (function_symbol * (llvm_ir option)) -> string = function
  | (fsymb, Some {code=il}) -> (string_of_proto true fsymb)^
                               "{\n"^(string_of_instr_list il)^"\n}"
  | (fsymb, None) ->   (string_of_proto false fsymb)^""
