type token =
    IDENT of string
  | TEXT of string
  | INTEGER of int
  | LP
  | RP
  | LB
  | RB
  | LC
  | RC
  | COM
  | PLUS
  | MINUS
  | MUL
  | DIV
  | ASSIGN
  | FUNC_KW
  | PROTO_KW
  | INT_KW
  | VOID_KW
  | RETURN_KW
  | PRINT_KW
  | READ_KW
  | IF_KW
  | THEN_KW
  | ELSE_KW
  | FI_KW
  | WHILE_KW
  | DO_KW
  | OD_KW
  | EOL
  | EOF


let to_string = function
  | IDENT s -> "IDENT of "^s
  | TEXT s -> "TEXT of "^s
  | INTEGER i -> "INTEGER of "^(string_of_int i)
  | LP -> "LP"
  | RP -> "RP"
  | LB -> "LB"
  | RB -> "RB"
  | LC -> "LC"
  | RC -> "RC"
  | COM -> "COM"
  | PLUS -> "PLUS"
  | MINUS -> "MINUS"
  | MUL -> "MUL"
  | DIV -> "DIV"
  | ASSIGN -> "ASSIGN"
  | FUNC_KW -> "FUNC_KW"
  | PROTO_KW -> "PROTO_KW"
  | INT_KW -> "INT_KW"
  | VOID_KW -> "VOID_KW"
  | RETURN_KW -> "RETURN_KW"
  | PRINT_KW -> "PRINT_KW"
  | READ_KW -> "READ_KW"
  | IF_KW -> "IF_KW"
  | THEN_KW -> "THEN_KW"
  | ELSE_KW -> "ELSE_KW"
  | FI_KW -> "FI_KW"
  | WHILE_KW -> "WHILE_KW"
  | DO_KW -> "DO_KW"
  | OD_KW -> "OD_KW"
  | EOL -> "\\n"
  | EOF -> "eof"

let prerr t = prerr_endline (to_string t)
