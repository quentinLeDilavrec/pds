open ASD
open Token

let parser_log (s:string) ((file,lnum,cnum,enum): string*int*int*int) =
    prerr_endline (">> "^s^" in "^file^" line: "^(string_of_int lnum)^" char: "^(string_of_int lnum)^"-"^(string_of_int enum))

(* p? *)
let opt p = parser
  | [< x = p >] -> parser_log "" __POS__; Some x
  | [<>] -> parser_log "" __POS__; None

(* p* *)
let rec many p = parser
  | [< x = p; l = many p >] -> parser_log "" __POS__; x :: l
  | [<>] -> parser_log "" __POS__; []

(* p+ *)
let some p = parser
  | [< x = p; l = many p >] -> parser_log "" __POS__; x :: l

(* p (sep p)* *)
let rec list1 p sep = parser
  | [< x = p; l = list1_aux p sep >] -> parser_log "list1" __POS__; x :: l
and list1_aux p sep = parser
  | [< _ = sep; l = list1 p sep >] -> parser_log "list1_aux.new_elem" __POS__; l
  | [<>] -> parser_log "list1_aux.nothing" __POS__; []

(* (p (sep p)* )? *)
let list0 p sep = parser
  | [< l = list1 p sep >] -> parser_log "list0" __POS__; l
  | [<>] -> parser_log "" __POS__; []

let rec program : Token.token Stream.t -> ASD.program = function | a -> match a with parser
  | [< e = func_or_proto [] ; _ = Stream.empty ?? ("unexpected input at the end:"^(String.concat " " (List.map Token.to_string (Stream.npeek 5 a)))) >] -> parser_log "program" __POS__; e

and func_or_proto (b:ASD.block list) : 'a -> ASD.block list = parser
  | [< '(Token.PROTO_KW); x=typ ?? "fop.proto" ; '(Token.IDENT y) ?? "fop.proto.ident" ;
       '(Token.LP) ?? "fop.proto.LP" ;
                       z=list0 expression comma ?? "fop.proto.params" ;
       '(Token.RP) ?? "fop.proto.RP" ;
       l = func_or_proto b >] -> ASD.Func(x,y,z,None)::l
  | [< '(Token.FUNC_KW); x=typ ?? "fop.func.typ" ; '(Token.IDENT y) ?? "fop.func.ident" ;
       '(Token.LP) ?? "fop.proto.LP" ;
                       z=list0 expression comma ?? "fop.proto.params" ;
       '(Token.RP) ?? "fop.proto.RP" ;
       bb = block ?? "fop.func.block";
       l = func_or_proto b >] -> ASD.Func(x,y,z,Some bb)::l
  | [< >] -> parser_log ">bloc_aux.nothing" __POS__; b

and block : 'a -> ASD.block list = parser
   | [< '(Token.LC) ; x = block_aux [] ?? "block.content" >] -> x
   | [< x = instr >] -> x::[]

and block_aux (i:ASD.block list) = parser
  | [< '(Token.RC) >] -> i
  | [< x0 = instr ; x = block_aux i >] -> parser_log "block_aux.chaining" __POS__; x0::x

and instr : 'a -> ASD.block= parser
  | [< '(Token.IF_KW); x=expression;
       '(Token.THEN_KW); y=block;
       z=(parser [<'(Token.ELSE_KW); z=block >] -> Some z | [<>] -> None) ;
       '(Token.FI_KW) >] -> ASD.Cond (x,y,z)
  | [< '(Token.WHILE_KW); x=expression;
       '(Token.DO_KW); y=block;
       '(Token.OD_KW) >] -> ASD.While (x,y)
  | [< '(Token.RETURN_KW); x=expression ?? "instr.return.1">] ->
           parser_log "instr.return" __POS__; ASD.Return(x)
  | [< '(Token.READ_KW); l=list0 expression comma ?? "instr.read.1" >] ->
           parser_log "instr.read" __POS__; ASD.Read(l)
  | [< '(Token.PRINT_KW); l=list0 expression comma ?? "instr.print.1" >] -> parser_log "instr.print" __POS__; ASD.Print(l)
  | [< x=typ; l=list0 expression comma ?? "instr.decl.1" >] -> parser_log "instr.decl" __POS__; ASD.Declaration(x,l)
  | [< x=expression;
       z=(parser
         | [< '(Token.ASSIGN); y=expression ?? "instr.assign.2" >] -> (parser_log "instr.assign" __POS__; ASD.Assign(x,y))
         | [<>] -> match x with
           | CallExpression (x,y) -> parser_log "instr.funcall" __POS__; ASD.Call(x,y)
           | _ -> failwith "oups") >] -> z

and expression : 'a -> ASD.expression = parser
  | [< e1 = factor; e = expression_aux e1 >] -> parser_log "expression" __POS__; e

and expression_aux (e1: ASD.expression) = parser
  | [< '(Token.PLUS);  e2 = factor ?? "expr_aux.1";
               e = expression_aux (ASD.AddExpression (e1, e2)) ?? "expr_aux.2" >] ->
         parser_log "expr_aux.+" __POS__; e
  | [< '(Token.MINUS);  e2 = factor; e = expression_aux (ASD.SubExpression (e1, e2)) >] -> parser_log ">expr_aux.-" __POS__; e
  | [< >] -> parser_log "expression_aux.nothing" __POS__; e1
  | [< 'a >] -> parser_log "expression_aux" __POS__;
                Token.print a;
                raise (Failure "in expr_aux");

and factor = parser
  | [< e1 = primary; e = factor_aux e1 >] -> parser_log "factor" __POS__; e

and factor_aux e1 = parser
  | [< '(Token.MUL);  e2 = factor; e = factor_aux (ASD.MulExpression (e1, e2)) >] -> parser_log "factor_aux.*" __POS__; e
  | [< '(Token.DIV);  e2 = factor; e = factor_aux (ASD.DivExpression (e1, e2)) >] -> parser_log "factor_aux./" __POS__; e
  | [<>] -> parser_log "factor_aux.empty" __POS__; e1

and typ = parser
  | [< '(Token.INT_KW) >] -> ASD.Type_Int
  | [< '(Token.VOID_KW) >] -> ASD.Type_Void

and primary : Token.token Stream.t -> ASD.expression = parser
  | [< '(Token.LP) ; x=expression ; '(Token.RP) >] -> parser_log (">primary.parnnt ") __POS__; x
  | [< '(Token.INTEGER x) >] -> parser_log (">primary.integer "^(string_of_int x)) __POS__; ASD.IntegerExpression x
  | [< '(Token.IDENT s) ; x=parametred s>] -> parser_log "primary.ident" __POS__; x
  | [< '(Token.TEXT s) >] -> parser_log "primary.text" __POS__; ASD.QuoteExpression s

and parametred (s:string): 'a -> ASD.expression= parser
  | [< '(Token.LB) ;
            x = (parser [< x=expression>] -> Some x | [<>] -> Some (IntegerExpression 0)) ;
       '(Token.RB)>] -> parser_log "indexed.t" __POS__; ASD.IdentExpression(s,x)
  | [< '(Token.LP) ;
            x = list0 expression comma ;
       '(Token.RP)>] -> parser_log "indexed.t" __POS__; ASD.CallExpression(s,x)
  | [< >] -> parser_log "indexed.f" __POS__; ASD.IdentExpression(s,None)

and comma = parser
  | [< 'COM >] -> parser_log "> COMMA" __POS__; ()
