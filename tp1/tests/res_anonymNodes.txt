Starting main
>	 check pretty printer with a basic ast
: ntriples -->
<s> <v> "c" .
<s> <v> <aa> .
<gg> <v> "c" .
<gg> <v2> <aa> .

: turtle -->
<s> <v> "c", <aa>.
<gg>
  <v> "c" ;
  <v2> <aa> .
>	check token stream:
entity: poly117 entity: version [entity: annee quote: 2007 ; entity: nbpages quote: 147 ], [entity: annee quote: 2011 ; entity: nbpages quote: 163 ]. eof 
>	 check pretty printer with stdin
: ntriples -->
<poly117> <version> _:v1 .
_:v1 <annee> "2007" .
_:v1 <nbpages> "147" .
<poly117> <version> _:v2 .
_:v2 <annee> "2011" .
_:v2 <nbpages> "163" .

: turtle -->
<poly117> <version> 
  [ <annee> "2007" ;
    <nbpages> "147" ] , 
  [ <annee> "2011" ;
    <nbpages> "163" ] .

: xml -->
<?xml version="1.0" encoding="UTF-8"?>
<rdf:RDF
  xml:base="http://mydomain.org/myrdf/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="poly117">
  <version rdf:parseType="Recource">
    <annee>2007</annee>
    <nbpages>147</nbpages>
  </version>
  <version rdf:parseType="Recource">
    <annee>2011</annee>
    <nbpages>163</nbpages>
  </version>
</rdf:Description>
</rdf:RDF>
