{
open Lexing
open Token

type error = {
  character: char;
  line: int;
  pos: int;
}

exception Unexpected_character of error
exception SyntaxError of string
}

(************************************************)

let blanks    = [' ' '\t' '\r' '\n' '\r']
let id = ['a'-'z' 'A'-'Z' '_'] ['a'-'z' 'A'-'Z' '0'-'9' '_' '-']*
let stringreg = ['a'-'z' 'A'-'Z' '0'-'9' '_' ' ' '-']*

rule tokenize = parse

  (* skip new lines and update line count 
       (useful for error location) *)
  | '\n'
      { let _ = new_line lexbuf in tokenize lexbuf }

  (* skip other blanks *)
  | blanks
      { tokenize lexbuf }


  (* Fill here! *)
  | "<" ([^ '>']* as str) ">" {(ENTITY str)::(tokenize lexbuf)}
  |'"' ([^ '"']* as str) '"' {(QUOTE str)::(tokenize lexbuf)}
  | "[" {LEFTB::(tokenize lexbuf)}
  | "]" {RIGHTB::(tokenize lexbuf)}
  | "." {DOT::(tokenize lexbuf)}
  | "," {COMMA::(tokenize lexbuf)}
  | ";" {SEMI::(tokenize lexbuf)}

  | eof {EOF::[]}

  (* catch errors *)
  | _ as c
    { let (e: error) = {
          character = c;
          line = lexbuf.lex_curr_p.pos_lnum;
          pos  = lexbuf.lex_curr_p.pos_cnum
                 - lexbuf.lex_curr_p.pos_bol
        } in
      raise (Unexpected_character e)
    }
and read_string buf =
  parse
  | '"'       { QUOTE (Buffer.contents buf) }
  | '\\' '/'  { Buffer.add_char buf '/'; read_string buf lexbuf }
  | '\\' '\\' { Buffer.add_char buf '\\'; read_string buf lexbuf }
  | '\\' 'b'  { Buffer.add_char buf '\b'; read_string buf lexbuf }
  | '\\' 'f'  { Buffer.add_char buf '\012'; read_string buf lexbuf }
  | '\\' 'n'  { Buffer.add_char buf '\n'; read_string buf lexbuf }
  | '\\' 'r'  { Buffer.add_char buf '\r'; read_string buf lexbuf }
  | '\\' 't'  { Buffer.add_char buf '\t'; read_string buf lexbuf }
  | [^ '"' '\\']+
    { Buffer.add_string buf (Lexing.lexeme lexbuf);
      read_string buf lexbuf
    }
  | _ { raise (SyntaxError ("Illegal string character: " ^ Lexing.lexeme lexbuf)) }
  | eof { raise (SyntaxError ("String is not terminated")) }

(*
  | '"' { let tmp = read_string (Buffer.create 17) lexbuf in tmp::(tokenize lexbuf) }
*)
