(* ASD type *)

type rdf = RDF of sentence list
and sentence = Sentence of entity * ((entity * (complement list)) list)
and entity = Entity of string
and complement = Anonym of (entity * (complement list)) list | CEntity of string | CQuote of string
(* type document = *)
  (* Fill here! *)

let entity2str = function | Entity e -> "<"  ^ e ^ ">"
let comp2str : complement -> string = function
    | CQuote q ->  "\"" ^ q ^ "\""
    | CEntity e -> "<"  ^ e ^ ">"
    | Anonym vcs -> ""
let format_line : string list -> string = function
  | [] -> ""
  | s::[] -> s
  | v::c::[] -> v ^ " " ^ c ^ " ;"
  | s::v::c::[] -> s ^ " " ^ v ^ " " ^ c ^ " ."
  | _ -> ""

(* Function to generate the document out of the AST *)
let ntriples_of_ast :rdf -> string =
  let rec comps ((s, v): string*entity) (i: int) (l: complement list) : string list =
    match l with
    | [] -> []
    | c::l -> match c with
    | CQuote q ->  (format_line [s ; entity2str v ; "\"" ^ q ^ "\"" ])::(comps (s,v) i l)
    | CEntity e -> (format_line [s ; entity2str v ; "<"  ^ e ^ ">"  ])::(comps (s,v) i l)
    | Anonym vcs ->
             (format_line [s;entity2str v;"_:" ^ (String.sub (let Entity s = v in s) 0 1) ^ (string_of_int i)])
             ::(String.concat "\n" (List.map (verbcomp ("_:" ^ (String.sub (let Entity s = v in s) 0 1) ^ (string_of_int i))) vcs))
             ::(comps (s, v) (i+1) l)
  and verbcomp (s: string) :  (entity * complement list) -> string = function
    | (v, cs) -> String.concat "\n" (comps (s, v) 1 cs)

  in let sent : sentence -> string = function
    | Sentence (s, vc) -> String.concat "\n" (List.map (verbcomp (entity2str s)) vc)
  in function
    | RDF(l) -> String.concat "\n" (List.map sent l)



let turtle_of_ast : rdf -> string =
  let rec comps = function
    | CQuote q ->  "\"" ^ q ^ "\""
    | CEntity e -> "<"  ^ e ^ ">"
    | Anonym vcs -> "\n  [ " ^
                    (String.concat " ;\n    " (List.map verbcomp vcs)) ^ " ] "
  and verbcomp :  (entity * complement list) -> string = function
    | (v, cs) -> (entity2str v) ^ " " ^ (String.concat ", " (List.map comps cs))
  in let sent = function
    | Sentence (s, (v, (Anonym c)::cs)::vcs) ->
                          entity2str s ^ " " ^ (String.concat "\n  " (List.map verbcomp ((v,(Anonym c)::cs)::vcs))) ^ "."
    | Sentence (s, vcs) when ((List.length vcs)=1) ->
                          entity2str s ^ " " ^ (String.concat "\n  " (List.map verbcomp vcs)) ^ "."
    | Sentence (s, vc) -> entity2str s ^ "\n  " ^
                          (String.concat " ;\n  " (List.map verbcomp vc)) ^ " ."
  in function
    | RDF(l) -> String.concat "\n" (List.map sent l)

                   (* (balise (let s = entity2str v in if s="type" then "rdf:"^s else s) *)
                           (* (match x with CEntity s -> [("rdf:recource",Some s)] | _ -> [])
                            * (match x with CQuote  s -> [s] | _ -> [])
                            * (i,0) *)

let xml_of_ast : rdf -> string =
  let entity2str = function | Entity e -> e
  in let attr2str : (string * string option) -> string = function
  | (a,None) -> a
  | (a, Some b) -> a^"=\""^b^"\""
  in let balise name handler attr cont ((i,j) : int*int) : string =
    (let first = ((String.make (i*2) ' ') ^ "<" ^ name)::(List.map attr2str attr)
     in let attrspacing = if (List.length attr)=0 then "" else if (String.length (String.concat " " first))+1>80
                          then ("\n"^(String.make ((i+1)*2) ' '))
                          else " "
    in (String.make (i*2) ' ') ^ "<" ^ name ^ attrspacing ^ (String.concat attrspacing (List.map attr2str attr)) ^
    (match cont with
    | [] -> "/>"
    | _ -> ">" ^
           (String.make j '\n') ^
             (String.concat (String.make j '\n') (List.map (handler (i+1)) cont ))^
           (String.make j '\n') ^ (String.make (i*j*2) ' ') ^ "</" ^ name ^ ">"))
  in let rec comp i v x : string =
    (let s = (let s = entity2str v in if s="type" then "rdf:"^s else s)
    in match x with
    | CQuote q -> balise s (fun i c -> c) [] [q] (i,0)
    | CEntity e -> balise s (fun i c -> c) [("rdf:resource", Some e)] [] (i,0)
    | Anonym vcs -> balise s verbcomp [("rdf:parseType",Some "Recource")] vcs (i,1))
  and verbcomp (i: int) :  'a -> string = function
    | (v, cs) ->
         (String.concat "\n" (List.map (comp i v) cs))
  in let sent i : sentence -> string = function
    | Sentence (s, vc) ->
         (balise "rdf:Description" verbcomp
                ["rdf:about",Some (let Entity str = s in str)]
                vc ((i-1),1))
  in function
    | RDF(l) ->
         "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" ^
         balise "rdf:RDF" sent
                ["xml:base",Some "http://mydomain.org/myrdf/";
                 "xmlns:rdf",Some "http://www.w3.org/1999/02/22-rdf-syntax-ns#"]
                l (0,1)

module EntityMap = Map.Make(String)
module EntitySet = Set.Make(String)
let opt2v v : 'a option -> 'a = function | None -> v | Some l -> l
let opt2nil : 'a option -> 'a = opt2v []

let filtering f (Entity verb) : rdf -> 'a -> sentence list =
  let comps : complement -> string =
    function
    | CQuote q -> "q"^q
    | CEntity e -> "e"^e
    | Anonym vcs -> ""
  in let vcgetkey : (entity * complement list) -> EntitySet.t = function
    | (Entity v, cs)  when (verb=v) -> EntitySet.of_list (List.map comps cs)
    | _ -> EntitySet.empty
  in let sentgetkey : sentence -> EntitySet.t = function
    | Sentence (_, vc) -> (List.fold_left
                               (fun acc x -> EntitySet.union acc (vcgetkey x))
                               EntitySet.empty vc)
  in let compilesent (acc: sentence list EntityMap.t) (s:sentence) : sentence list EntityMap.t =
          let keys = sentgetkey s
          in EntitySet.fold
                  (fun y (acc: sentence list EntityMap.t) ->
                         EntityMap.add y (s::((EntityMap.find_opt verb acc) |> opt2nil)) acc)
                  keys acc
  in (function
    | RDF(l) ->
       let d : sentence list EntityMap.t =
           List.fold_left compilesent
             (EntityMap.empty: sentence list EntityMap.t) l
             in f d)
let filter : entity -> rdf -> complement -> sentence list =
       filtering
            (fun d -> function
               | CEntity x -> ((EntityMap.find_opt ("e"^x) d) |> opt2nil)
               | CQuote x -> ((EntityMap.find_opt ("q"^x) d) |> opt2nil))
and verbfilter e r: sentence list =
       filtering
            (fun d -> function
               | _ -> [])
            e r ()


let get s v : rdf -> complement list =
  let verbcomp : (entity * complement list) -> complement list = function
    | (Entity verb, cs)  when (verb=v) -> cs
    | _ -> []
  in let sent : sentence -> complement list = function
    | Sentence (Entity _s, vc) when (_s=s) -> List.concat (List.map verbcomp vc)
    | _ -> []
  in (function
    | RDF(l) -> List.concat (List.map sent l))
