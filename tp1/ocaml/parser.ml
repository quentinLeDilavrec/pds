open ASD
open Token

exception Unexpected_token of string
exception Unexpected_eof of string
let to_str l = String.concat " " (List.map Token.to_str l)
let parse (tokens:Token.token Stream.t): ASD.rdf =
  let rec entity_or_quote s : ASD.complement = match s with parser
  | [< '(Token.ENTITY c) >] -> (ASD.CEntity c)
  | [< '(Token.QUOTE c) >] ->  (ASD.CQuote c)
  and comp s : ASD.complement list= match s with parser
  | [< '(Token.COMMA); x=bracket; y=comp >] ->
                  x::y
  | [<  >] -> []
  and vcomps s : (ASD.entity * (ASD.complement list)) list = match s with parser
  | [< '(Token.SEMI); x=vcomp; y=vcomps ?? "vcomps.y">] ->
                   x::y
  | [< >] ->  []
  and vcomp s : (ASD.entity * (ASD.complement list)) = match s with parser
  | [< '(Token.ENTITY v) ; x=bracket ; y=comp >] ->
                   (ASD.Entity v, x::y)
  and bracket s : ASD.complement= match s with parser
  | [< '(Token.LEFTB);
             x=vcomp ?? "bracket.1.x"; y=vcomps ?? "bracket.1.y";
       '(Token.RIGHTB) ?? "bracket.1.RIGHTB" >] ->
                  Anonym (x::y)
  | [< c=entity_or_quote >] -> c
  and sent s : ASD.sentence list = match s with parser
  | [< '(Token.ENTITY s); x=vcomp; y=vcomps ?? "y"; z=dot >] ->
                  (ASD.Sentence ((ASD.Entity s), x::y))::z
  | [< '(Token.EOF) >] ->  []
  | [< '(Token.DOT) >] ->  []
  | [< >] ->  []
  and dot s = match s with parser
  | [< '(Token.DOT); x=sent ?? "dot">] ->  x
  | [< '(Token.DOT) >] ->  []
  in match tokens with parser
  | [< '(Token.ENTITY s); x=vcomp ; y=vcomps ?? "y"; z=dot ?? "z">] ->
                 ASD.RDF ((ASD.Sentence ((ASD.Entity s), x::y))::z)
  | [< '(Token.EOF) >] ->  ASD.RDF []
  | [<  >] ->  ASD.RDF []
(*
let rec parse tokens =
let compcom i = match Stream.peek tokens with
  | None   -> raise (Unexpected_eof "compcom")
  | Some e -> match e with
  | SEMI  -> ()
  | DOT   -> ()
  | COMMA ->  Stream.junk tokens; ()
  | e -> raise (Unexpected_token "compcom")
in let rec comp i = match Stream.peek tokens with
  | None   -> raise (Unexpected_eof "comp")
  | Some e -> match e with
  | ENTITY s -> Stream.junk tokens; (compcom (i+1)); (ASD.CEntity s)::(comp (i+1))
  | QUOTE s  -> Stream.junk tokens; (compcom (i+1)); (ASD.CQuote  s)::(comp (i+1))
  | COMMA -> Stream.junk tokens; []
  | SEMI  -> []
  | DOT   -> []
  | e -> raise (Unexpected_token "comp")
in let rec vcomp i = match Stream.peek tokens with
  | None   ->  raise (Unexpected_eof "vcomp")
  | Some e ->  match e with
  | ENTITY s -> 
      let tmp = comp (i+1) in (ASD.Entity s, tmp)::(vcomp (i+1))
  | SEMI -> Stream.junk tokens; []
  | DOT  -> []
  | e -> raise (Unexpected_token "vcomp")
in let rec sent i = match Stream.next tokens with
  | ENTITY s ->  let tmp = vcomp (i+1) in (ASD.Sentence (Entity s, tmp))::(sent (i+1))
  | DOT -> []
  | e -> raise (Unexpected_token "sent")
in
RDF (sent 0)

*)



(*  Fill here! *)
