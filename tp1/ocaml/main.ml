open Lexer
open Parser
open ASD

print_string "Starting main\n"

let a = (Stream.of_list
[(Token.ENTITY "s");
  (Token.ENTITY "v");(Token.ENTITY "c"); (Token.COMMA);
    (Token.QUOTE "last"); (Token.COMMA);
    (Token.ENTITY "last"); (Token.SEMI);
  (Token.ENTITY "v");(Token.ENTITY "c"); Token.DOT])

let stream_tee stream =
    let next self other i =
      try
        if Queue.is_empty self
        then
          let value = Stream.next stream in
          Queue.add value other;
          Some value
        else
          Some (Queue.take self)
      with Stream.Failure -> None in
    let q1 = Queue.create () in
    let q2 = Queue.create () in
    (Stream.from (next q1 q2), Stream.from (next q2 q1));;


let () =
  (* Use with a manually made AST *)
  let ast = (RDF [(Sentence (Entity "s", [(Entity "v", [CQuote "c"; CEntity "aa"])]));
                  (Sentence (Entity "gg", [(Entity "v", [CQuote "c"]); (Entity "v2", [CEntity "aa"])]))])
  in begin
    print_endline ">\t check pretty printer with a basic ast";
    print_endline ": ntriples -->";
    print_endline (ASD.ntriples_of_ast ast);
    print_newline ();
    print_endline ": turtle -->";
    print_endline (ASD.turtle_of_ast ast),
    print_newline ();
    print_endline ": xml -->";
    print_endline (ASD.xml_of_ast ast)
  end

  (* Use with lexer and parser *)
  let lexbuf = Lexing.from_channel stdin
  in try
    let token_stream : Token.token Stream.t = Stream.of_list (Lexer.tokenize lexbuf)
    in let token_stream, copy = stream_tee token_stream
         in print_endline ">\tcheck token stream:"; Stream.iter Token.print copy ; print_newline ();
         let ast: ASD.rdf = Parser.parse token_stream
    in let result_ntriples = ASD.ntriples_of_ast ast
    and result_turtle = ASD.turtle_of_ast ast
    and result_xml = ASD.xml_of_ast ast
    and preset_filter = ASD.filter (ASD.Entity "type") ast
    in
      print_endline ">\t check pretty printer with stdin (test1/2.ttl) also work with anonym nodes";
      print_endline ": ntriples -->";
      print_endline result_ntriples;
      print_newline ();
      print_endline ": turtle -->";
      print_endline result_turtle;
      print_newline ();
      print_endline ": xml -->";
      print_endline result_xml;
      print_newline ();
      print_endline ": filter sentence with of nom Ferre -->";
      print_endline (ASD.turtle_of_ast (ASD.RDF (ASD.filter (ASD.Entity "nom") ast (ASD.CQuote "Ferre"))));
      print_newline ();
      print_endline ": preset filter, get sentence of type parcours -->";
      print_endline (ASD.turtle_of_ast (ASD.RDF (preset_filter (ASD.CEntity "parcours"))));
      print_endline ": preset filter; get sentence of type formation -->";
      print_endline (ASD.turtle_of_ast (ASD.RDF (preset_filter (ASD.CEntity "formation"))));
      print_newline ();
      print_endline ": get complements by subject and verb -->";
      print_endline ":   m1info parcours -->";
      List.iter (fun x -> print_endline (ASD.comp2str x)) (ASD.get "m1info" "parcours" ast);
      print_newline ();
      print_endline ":   m1infoRI intitule -->";
      List.iter (fun x -> print_endline (ASD.comp2str x)) (ASD.get "m1infoRI" "intitule" ast)
  with Lexer.Unexpected_character e ->
  begin
    Printf.printf
      "Unexpected character: '%d' at position '%d' on line '%d'\n"
      (Char.code e.character) e.pos e.line;
    exit 1
  end

(* qui à fait quels poly / generer *)
(* list des cours par formations / generer *)

(* get Sent filtered by type formation *)
