type token =
| DOT | COMMA | SEMI
| ENTITY of string
| QUOTE of string
| EOF
| LEFTB | RIGHTB

let rec to_str : token -> string = function
  | DOT -> ". "
  | COMMA -> ", "
  | SEMI -> "; "
  | EOF -> "eof "
  | ENTITY s -> ("entity: " ^ s ^ " ")
  | QUOTE  s -> ("quote: " ^ s ^ " ")
  | LEFTB -> "["
  | RIGHTB -> "]"

let rec print t = t |> to_str |> print_string
